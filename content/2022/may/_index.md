---
title: "Open Research Webinars"
headline: "Open Research Webinars"
subtitle: "Open collaboration in European research projects"
tagline: "Next event: Tuesday, May 3, 2022, 16:00-17:00 CET"
date: 2022-05-03T16:00:00+02:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-default-event header-open-research-europe-event"
hide_breadcrumb: true
container: container-fluid
summary: "Open Research Webinars is a series presenting software research projects that are helping to shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs."
links: [[href: "#speakers", text: Speakers], [href: "#agenda", text: Agenda TBD]]
layout: single
---

{{< grid/section-container id="registration" class="featured-section-row featured-section-row-lighter-bg" >}}
   {{< events/registration event="may" year="2022" title="About the Event" >}} 

Join us for our next Open Research Webinars on Tuesday, May 3, 2022 at 16:00 CET. 

Through a selection of state-of-the-art project presentations and demonstrations, this new series of webinars introduces software research projects that help shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs.

### Presentations   



   {{</ events/registration >}}
{{</ grid/section-container >}}

{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="may" year="2022" title="Speakers" source="speakers" imageRoot="/2022/may/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/agenda event="may" year="2022" >}}
{{</ grid/section-container >}}

{{< co-organizers >}}
{{< past_events >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}