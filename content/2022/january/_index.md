---
title: "Open Research Webinars"
headline: "Open Research Webinars"
subtitle: "Open collaboration in European research projects"
tagline: "Next event: Tuesday, January 18, 2022, 16:00-17:00 CET"
date: 2021-09-28T16:00:00+02:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-default-event header-open-research-europe-event"
hide_breadcrumb: true
container: container-fluid
summary: "Open Research Webinars is a series presenting software research projects that are helping to shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs."
links: [[href: "#speakers", text: Speakers], [href: "#agenda", text: Agenda], [href: "https://www.eventbrite.fr/e/open-research-webinar-january-18-tickets-220695956547", text: Register]]
layout: single
---

{{< grid/section-container id="registration" class="featured-section-row featured-section-row-lighter-bg" >}}
   {{< events/registration event="january" year="2022" title="About the Event" >}} 

Join us for our next Open Research Webinars on Tuesday, January 18, 2022 at 16:00 CET. 

Through a selection of state-of-the-art project presentations and demonstrations, this new series of webinars introduces software research projects that help shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs.

### Presentations   
[![Eclipse STEADY](images/steady.png)](https://eclipse.org/steady) [![Sat4J](images/sat4j.png)](https://www.sat4j.org/)

   {{</ events/registration >}}
{{</ grid/section-container >}}

{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-dark-bg eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="january" year="2022" title="Speakers" source="speakers" imageRoot="/2022/january/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/agenda event="january" year="2022" >}}
{{</ grid/section-container >}}

{{< co-organizers >}}
{{< past_events >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}