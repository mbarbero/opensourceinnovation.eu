---
title: "Open Research Webinars"
headline: "Open Research Webinars"
subtitle: "Open collaboration in European research projects"
tagline: ""
date: 2020-12-15T16:00:00+02:00
summary: "Join us for the launch of Open Research Webinars on Tuesday, December 15, 2020 at 16:00"
layout: "single"
hide_page_title: true
hide_sidebar: true
container: container-fluid
---

{{< grid/section-container id="about" class="featured-section-row featured-section-row-lighter-bg" >}}
{{< grid/section-container class="featured-section-row text-center">}}
<h1>About the Webinars</h1>
Through a selection of state-of-the-art project presentations and demonstrations, this new series of webinars introduces software research projects that help shape the future of open source software and the IT industry. Co-organized by OW2 and the Eclipse Foundation, the webinars will focus on international partners leveraging open source in European publicly-funded collaborative research and innovation programs.

{{</ grid/section-container >}}
{{</ grid/section-container >}}

{{< grid/section-container id="about" class="featured-section-row featured-section-row-dark-bg" >}}
{{< grid/section-container class="featured-section-row text-center">}}

<h2>Upcoming Event: <a href="2022/may"> May 3, 2022</h2></a>
<!--
<a href="https://eclipse.org/steady" target="_blank"><img src="2022/january/images/steady.png" width="200" alt="Steady"></a>
<a href="https://www.sat4j.org/" target="_blank"><img src="2022/january/images/sat4j.png" width="200" alt="Sat4j"></a>
-->

{{</ grid/section-container >}}
{{</ grid/section-container >}}



{{< grid/section-container id="about" class="featured-section-row featured-section-row-lighter-bg" >}}
{{< grid/section-container class="featured-section-row text-center">}}
<h2>Presented projects<h2>
<a href="2022/january"><img src="2022/january/images/sat4j.png" width="100" alt="Sat4j"></a>
<a href="2022/january"><img src="2022/january/images/steady.png" width="100" alt="Steady"></a>
<a href="2021/september"><img src="2021/september/images/activeeon.png" width="100" alt="ActiveEon"></a>
<a href="2021/june"><img src="2021/june/images/basyx.png" width="100" alt="BaSyx"></a>
<a href="2021/september"><img src="2021/september/images/capra.png" width="100" alt="capra"></a>
<a href="2021/march"><img src="2021/march/images/decoder.png" width="100" alt="DECODER"></a>
<a href="2021/june"><img src="2021/june/images/fasten.png" width="100" alt="FASTEN"></a>
<a href="2021/march"><img src="2021/march/images/pdp4e.png" width="100" alt="PDP4E"></a>
<a href="2020/december"><img src="2020/december/images/reachout.png" width="100" alt="ReachOut!"></a>
<a href="2020/december"><img src="2020/december/images/smartclide.png" width="100" alt="SmartCLIDE"></a>

{{</ grid/section-container >}}
{{</ grid/section-container >}}

{{< past_events >}}
